-- restaurants

CREATE TABLE restaurants (
    id text primary key,
    name text,
    address text,
    rating real
);

insert into restaurants (id, name, address, rating)
values ('db1f7413-9e8b-4265-afa7-b106d42635e1', 'Voldemort Potions', 'hogwarts', 3),
        ('84705f13-c591-4983-b370-f22b34dba605', 'Beckham Fastfood', 'miami', 4.5),
        ('7d822181-5af4-49d4-a337-c76737d75b36', 'Wonder Woman Haleem', 'azadi st.', 5),
        ('a68cac63-0892-4fb1-b632-716afb0883a0', 'AquaMan Sea Foods', 'atlantis', 2.5);

-- foods

create table foods (
    id text primary key,
    restaurant_id text references restaurants(id)
        on delete restrict
        on update cascade,
    name text,
    price integer
);

insert into foods (id, restaurant_id, name, price)
values ('fe3d1a38-375d-42c9-b3eb-8c374d0e172c', 'db1f7413-9e8b-4265-afa7-b106d42635e1', 'Face changing potion', 10500),
    ('a95cb27e-8a42-41df-84cc-4d2e9ab000ae', 'db1f7413-9e8b-4265-afa7-b106d42635e1', 'Invisibility potion', 14000),
    ('cdaffb2f-b969-4c1b-a6a0-fd07c1ba3501', 'a68cac63-0892-4fb1-b632-716afb0883a0', 'Shrimp from atlantis', 22000),
    ('04b5bc8e-1c20-437c-9e2c-3e5213af964b', '7d822181-5af4-49d4-a337-c76737d75b36', 'Amazonian baked beans', 5500);

-- users

CREATE TABLE users (
    id text primary key,
    firstname text,
    lastname text,
    password text,
    credit integer,
    unique (firstname, lastname)
);

insert into users (id, firstname, lastname, password, credit)
values ('ec0f3885-6121-44b5-8d20-9ce47ff35ebe', 'mohammad', 'ghanbari', 'mhdgh', 15000),
    ('5c54e032-747b-4597-99b4-a5f09c3a32c6', 'hosein', 'norouzi', 'hsnrz', 10200),
    ('9f071bb6-9138-4205-98c1-a69fdcf57aae', 'javad', 'vahedi', 'jvdvhd', 0),
    ('fe569083-7bdd-45b2-8ced-137e6db599da', 'amirhosein', 'ebrahimi', 'amirhe', 30000),
    ('b8657678-90bf-4f74-a0c4-2eb3081b9805', 'amirhosein', 'shafie', 'amirsh', 0);

-- orders

create table orders (
    id text primary key,
    restaurant_id text references restaurants (id),
    date timestamp
);

create table order_item (
    order_id text references orders (id),
    food_id text references foods (id),
    quantity integer,
    unit_price integer,
    primary key (order_id, food_id)
);