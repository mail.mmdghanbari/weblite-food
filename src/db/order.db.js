const format = require('pg-format')
const { v4: uuidv4 } = require('uuid')
const { getClient } = require('./index')
// db
const { getUserCredit, addUserCredit } = require('./user.db')

exports.submitOrder = async (userId, restaurantId, orderItems) => {
  const totalPrice = orderItems.reduce(
    (sum, { price, quantity }) => sum + price * quantity,
    0,
  )

  const client = await getClient()
  try {
    await client.query('begin')
    const userCredit = await getUserCredit(userId, client)

    if (totalPrice > userCredit) throw 'CREDIT_NOT_ENOUGH'

    const orderId = uuidv4()
    await client.query(
      `
        insert into orders (id, user_id, restaurant_id, date)
        values ($1, $2, $3, $4)
      `,
      [orderId, userId, restaurantId, new Date()],
    )

    const orderItemsToSave = orderItems.map(({ id, price, quantity }) => [
      orderId,
      id,
      quantity,
      price,
    ])
    await client.query(
      format(
        `
          insert into order_items (order_id, food_id, quantity, unit_price)
          values %L
        `,
        orderItemsToSave,
      ),
    )

    await addUserCredit(userId, -totalPrice, client)

    await client.query('commit')
  } catch (e) {
    client.query('rollback')
    console.error(e)
    throw e
  } finally {
    client.release()
  }
}
