const { query } = require('./index')
const format = require('pg-format')
const { v4: uuidv4 } = require('uuid')

exports.getRestaurantFoods = restId =>
  query(
    `
      select name, price from foods
      where restaurant_id = $1
    `,
    [restId],
  ).then(({ rows }) => rows)

exports.getFoodById = id =>
  query(
    `
      select restaurant_id as "restaurantId", id, name, price
      from foods
      where id = $1
    `,
    [id],
  ).then(({ rows }) => rows[0])

exports.getFoodsByIds = ids =>
  query(
    format(
      `
        select id, name, price from foods
        where id in (%L)
      `,
      ids,
    ),
  ).then(({ rows }) =>
    rows.reduce(
      (acc, food) => ({
        ...acc,
        [food.id]: food,
      }),
      {},
    ),
  )

exports.saveFood = ({ restaurantId, name, price }) =>
  query(
    `
      insert into foods (id, restaurant_id, name, price)
      values ($1, $2, $3, $4)
    `,
    [uuidv4(), restaurantId, name, price],
  )
