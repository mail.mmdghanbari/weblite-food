const { v4: uuidv4 } = require('uuid')
const { query } = require('./index')

exports.getRestaurants = () =>
  query(`
    select * from restaurants
    order by rating desc
  `).then(({ rows }) => rows)

exports.getRestaurantById = id =>
  query(
    `
      select * from restaurants
      where id = $1
    `,
    [id],
  ).then(({ rows }) => rows[0])

exports.saveRestaurant = ({ name, address, rating }) =>
  query(
    `
      insert into restaurants (id, name, address, rating)
      values ($1, $2, $3, $4)
    `,
    [uuidv4(), name, address, rating],
  )
