exports.getUserCredit = (userId, client) =>
  client
    .query(
      `
        select credit from users
        where id = $1
      `,
      [userId],
    )
    .then(({ rows }) => rows[0].credit)

exports.addUserCredit = (userId, amount, client) =>
  client.query(
    `
      update users
      set credit = credit + $1
      where id = $2
    `,
    [amount, userId],
  )
