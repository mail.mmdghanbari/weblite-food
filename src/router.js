const express = require('express')
// db
const {
  getRestaurants,
  getRestaurantById,
  saveRestaurant,
} = require('./db/restaurant.db')
const {
  getRestaurantFoods,
  getFoodById,
  getFoodsByIds,
  saveFood,
} = require('./db/food.db')
const { submitOrder } = require('./db/order.db')

const router = express.Router()

router.get('/restaurant', async (req, res) => {
  const restaurants = await getRestaurants()
  res.send(restaurants)
})

router.get('/restaurant/:id', async (req, res) => {
  const { id } = req.params
  const restaurant = await getRestaurantById(id)
  if (!restaurant) {
    res.sendStatus(404)
    return
  }

  const foods = await getRestaurantFoods(id)
  res.send({ ...restaurant, foods })
})

router.post('/restaurant', async (req, res) => {
  const restaurant = req.body
  await saveRestaurant(restaurant)
  res.sendStatus(200)
})

router.get('/food/:id', async (req, res) => {
  const { id } = req.params
  const food = await getFoodById(id)

  if (!food) res.sendStatus(404)
  else res.send(food)
})

router.post('/food', async (req, res) => {
  const food = req.body
  await saveFood(food)
  res.sendStatus(200)
})

// orderItems: [{ foodId, quantity }]
router.post('/order', async (req, res) => {
  try {
    const { userId, restaurantId, orderItems } = req.body
    const foods = await getFoodsByIds(orderItems.map(({ foodId }) => foodId))
    const orderItemsWithPrice = orderItems.map(({ foodId, quantity }) => ({
      ...foods[foodId],
      quantity,
    }))
    await submitOrder(userId, restaurantId, orderItemsWithPrice)
    res.sendStatus(200)
  } catch (e) {
    if (e === 'CREDIT_NOT_ENOUGH') res.status(400).send(e)
    else res.sendStatus(500)
  }
})

module.exports = router
