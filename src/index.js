const express = require('express')
const router = require('./router')

const app = express()
const port = 3000

app.use(express.json()) // for parsing application/json
app.use('/', router)

app.listen(port, () => {
  console.log(`server listening at port: ${port}`)
})
